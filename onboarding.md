# First Day

## Company Email
Make sure you already got your email account created and successfully accessible, provided by **Bizzy Digital IT Support Team**.

If you find your email account is typo and/or your details profile are needs to be revised, please contact these guys for further assistance:

* Wisnu (wisnu.prayogo@bizzy.co.id)

I assume everyone able to configure email account in any type of email clients, both desktop and mobile. If not, [Google](https://www.google.com/) yourself.

## Distribution List
Our list:

* sysdevops@bizzy.co.id

List owner:

* bizzydistribution.devops@bizzy.co.id

Send your request for your email account to be added to Bizzy Distribution Infrastructure Team Distribution List to:

* Wisnu (wisnu.prayogo@bizzy.co.id)

## Slack
... is no longer used, we eventually moving to [Mattermost](https://mattermost.bizzy.co.id)

## Mattermost
Please [sign up here](https://mattermost.bizzy.co.id/signup_user_complete/?id=6k8zqzzmftnkzcxj8zzxhhkaby) and join **Sysdevops** and **Tech Engineer** channel at the very first stage.

## Atlassian
Please get your account prepared by sending your email account details to:

* Sysdevops Team (sysdevops@bizzy.co.id)

Once you account active, please make sure you are able to access all of these pages at minimum:

* [Sysdevops Works - Board](https://sinarmas-distribusi.atlassian.net/secure/RapidBoard.jspa?projectKey=SYDOPS&rapidView=71)
* [Sysdevops Works - Wiki](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SW/overview)
* [Weekly Active Task & Retro](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SW/pages/926318607/Weekly+Catch-up)
* [SysDevOps Infra Wiki & How To](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/overview)
* [Crowdsys DevOps Wiki (mostly done by Pyco)](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/428507156/Crowdsys%27+DevOps+Information)
* [Code Repositories](https://bitbucket.org/crowdsys/) & [Projects](h://bitbucket.org/crowdsys/profile/projects)

If you can not, please reach this guy by providing your email & Bitbucket user account name details:

* Sysdevops Team (sysdevops@bizzy.co.id)

## Amazon AWS

You will need IAM, please send your email account details to:

* Sysdevops Team (sysdevops@bizzy.co.id)

Reference:

- [How-To New IAM](.aws-iam.md)
- [List of IAM Users](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/361693531/IAM+Users)

## sysPass

Master password: `9KTsE)JKIfS%!`

We store ~~various~~ all-you-need credentials here, please reach these guys by providing your email account details for further invitation:

* Sysdevops Team (sysdevops@bizzy.co.id)

Reference: [to be prepared](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/381026309/sysPass+Password+Manager)
