# Repository

## Important Repos

Make sure every team member are able to clone all repositories in [this](https://bitbucket.org/account/user/crowdsys/projects/CROW) and [this](https://bitbucket.org/account/user/crowdsys/projects/SYDPS) Project.

Contact:

- Sysdevops Team (sysdevops@bizzy.co.id)
