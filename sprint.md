# Sprint & Assignment

## General Information
* [Sysdevops Works](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SW/overview)
* [Sysdevops Infra](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/overview)

## Distribution

### Services
- TF State: `terraform/Services/init-infra-services/terraform-master.tfstate`

### CMS & Website Maintenance
- PM: Gloria
- Devops: Alter & Ade 
- TF State: to be migrated

### Farmer App
- PM: Emily & Gloria
- Devops: Alter & Ade (to be replaced with Dimas)
- TF State: `terraform/Crowdsys-apps/init-infra-farmer/terraform-master.tfstate`

### SSA/Tokosmart
- PM: Nadia
- Devops: Alter & Ade 
- TF State: n/a (TF only for provisioning infrastructure for the first time & Jenkins is for deployment)

### SSA/NU Commerce
- PM: Nadia
- Devops: Alter & Ade 
- TF State: 

### TMS
- PM: Batara, Haikal, Puspita & Anthony
- Devops: Hardy & Farda
- TF State: `terraform/TMS/init-infra-TMS/terraform-master.tfstate`

### Sales Order Management Platform (was SDS, infra-init merged to init-infra-sales-order-service)
- PM: Adi & Putri Aisyah 
- Devops: Ade & Dado
- TF State: `terraform/salesorder/init-infra-sales-order-service/terraform-master.tfstate`

### Daily Commercial Report/Principal Report Portal
- PM: Wiltonson
- Devops: Farda & Ayu
- TF State: `terraform/Daily-Commercial-Report/init-infra-dcr/terraform.tfstate`

### IDSP Control Tower
- PM: Wiltonson & Astrid
- Devops: Farda & Ayu
- TF State: `terraform/IDSPcontroltower/init-infra-idspcontroltower/terraform-master.tfstate`

### Master Data Management (MDM)
- PM: Jonathan
- Devops: Ade & Dado
- TF State: `terraform/Master_Data_Management/init-infra-mdm/terraform-master.tfstate`

### Pricing Management Portal (PMP)
- PM: Khonsa & Cita
- Devops: Alter & Hardy
- TF State: `terraform/Portal_Promo_Price/init-infra-pmp/terraform-master.tfstate`

## Logistics

### Truckway
- PM: Andre (andrey.angkoso@bizzy.co.id)
- Devops: Hardy (hardy.agustian@bizzy.co.id)
- TF State: updating (infra-init-truckway - terraform/truckway//terraform.tfstate)

## Marketplace, Consolidation & Enterprise

### Marketplace
- PM: Chintia
- Devops: n/a
- TF State: `terraform/sysdevops-comm-iac/infra-init-newmplace/terraform.tfstate`

### Consolidation
- PM: Chintia
- Devops: n/a
- TF State: n/a

### Enterprise
- PM: Chintia
- Devops: n/a
- TF State: n/a

