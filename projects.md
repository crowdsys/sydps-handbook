# Projects

## Visibility Traffic & Utilization

### [Health Checks](https://console.aws.amazon.com/route53/healthchecks/home#/)

![health_checks](https://s3-ap-southeast-1.amazonaws.com/bizzy-distribution-iac/sydps-handbook-images/ss_route53.png)

### [Grafana](https://bizzydist.grafana.net)

![grafana](https://s3-ap-southeast-1.amazonaws.com/bizzy-distribution-iac/sydps-handbook-images/ss_grafana.png)

### PMM [Percona Monitoring & Management](http://52.76.212.254/graph/d/Fxvd1timk/home-dashboard?orgId=1)

## Instance/Services Metrics & Logs

### [Cloudwatch](https://ap-southeast-1.console.aws.amazon.com/cloudwatch/home?region=ap-southeast-1#dashboards:)

![cloudwatch_logs](https://s3-ap-southeast-1.amazonaws.com/bizzy-distribution-iac/sydps-handbook-images/ss_cloudwatch_logs.png)
![cloudwatch_metrics](https://s3-ap-southeast-1.amazonaws.com/bizzy-distribution-iac/sydps-handbook-images/ss_cloudwatch_metrics.png)

## Event History & Trails

### [CloudTrail](https://ap-southeast-1.console.aws.amazon.com/cloudtrail/home?region=ap-southeast-1)

![cloudtrail_events](https://s3-ap-southeast-1.amazonaws.com/bizzy-distribution-iac/sydps-handbook-images/ss_cloudtrail_events.png)

## Team Communication

### [Mattermost](https://mattermost.bizzy.co.id/login)

## Network Designer & Labs

### [eve Next Generation]http://evelabs.tokosmart.id/#/login()

## On Research & Trial

- [Elasticsearch APM UI](https://70e6e02f361f409f8861cae9bb728ef1.ap-southeast-1.aws.found.io:9243/)
- [Serverless Blue Green Deployment](https://hackernoon.com/serverless-stack-ci-cd-blue-green-deployments-a6de63142094)

## On Hold

- [New Relic](https://rpm.newrelic.com/accounts/2192083/applications)
- [Datadog](https://app.datadoghq.com/functions)
- [AWS X-Ray](https://ap-southeast-1.console.aws.amazon.com/xray/home)
