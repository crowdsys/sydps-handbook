# Backlog

[List of Backlog](https://sinarmas-distribusi.atlassian.net/secure/RapidBoard.jspa?projectKey=SYDPS&rapidView=33&view=planning)

## Collaboration

Anyone are allowed to write down their idea related to solution, improvement and nice-to-have research idea in `Backlog`.

## Epic

If not sure, kindly ignore the `Epic`, just add in `Backlog`.

## Notes

* No limit, put everything
* Do not worry about `Duplicate` idea, to do, or heads up, we have **Retrospective** anyway