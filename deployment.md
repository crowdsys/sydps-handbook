# Continuous Delivery

## Pipeline
We use [Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/javascript-node-js-with-bitbucket-pipelines-873891287.html) for building, testing & deploy the Nodejs applications.

## Container
We build and test the Nodejs application inside our own [Docker Container](https://cloud.docker.com/u/bizzydist/repository/docker/bizzydist/dev_node) before published to particular environment (Dev, Stage, and Production).

## Production Annotation
We annotate Production deployment in Grafana and tag its activity as `deploy`.
