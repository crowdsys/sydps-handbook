# Legacy System & Stack

## Jenkins & Sonarqube
All access in [sysPass](https://secure.tokosmart.id).

Reference:

* [Jenkins Server](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/323485890/Jenkins+servers)
* [Sonarqube Server](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/351568029/Sonarqube+server)

## Reading Materials

* [Infrastructure](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/174129218/Infrastructure)
* [Accessing Servers](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/168591450/Environments)
* [CI/CD Pipeline (Development Env.)](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/173932735/CI+CD+pipeline+for+DEV+enviroment)
* [How-To Articles](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/410026184/How-to+Articles)

## Handover
Maintained actively as per xx February 2019 by:

- Alter (alter.mulyadi@bizzy.co.id)
- Ade (ade.hendrata@bizzy.co.id)
