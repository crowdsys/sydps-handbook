# sydps-handbook

- [Onboarding](./onboarding.md)
- [Tech Team](./whoiswho.md)
- [Reporting](./reporting.md)
- [AWS IAM](./aws-iam.md)
- [IaC](./iac.md)
- [Projects](./projects.md)
- [Backlog](./backlog.md)
- [Sprint](./sprint.md)
- [Deployment](./deployment.md)
- [SSA Legacy System](./legacy.md)
- [Sharing Session](./sharing.md)

# Todo

- AWS CloudFormation templating & designed via Serverless Framework (not direct create/edit in CloudFormation web console)
- Logging serverity to ERROR and CRITICAL for Elasticsearch APM Logs 
