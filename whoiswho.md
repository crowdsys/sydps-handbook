# Tech Team (and Product)
Should we know each other since the first month.

## CTO Group

* Denny Jansen (denny.jansen@bizzy.co.id)

## Development

* Fariz (fariz.darmaniar@bizzy.co.id)
* Umar (ahmad.umar@bizzy.co.id)
* Dwikky (dwikky.yudakusuma@bizzy.co.id)
* Danang (danang.iswadi@bizzy.co.id)
* Ricky (ricky.alexander@bizzy.co.id)
* Irwin (irwin.pratajaya@bizzy.co.id)

## SysDevOps

* Mirasz (cecep.albari@bizzy.co.id)
* Alter (alter.mulyadi@bizzy.co.id)
* Hardy (hardy.agustian@bizzy.co.id)
* Ade (ade.hendrata@bizzy.co.id)
* Dado (dadang.hardiputra@bizzy.co.id)
* Farda (farda.prasetyo@bizzy.co.id)
* Ayu (cynthia.windani@bizzy.co.id)
* Dimas (dimas.putra@bizzy.co.id)

## Our Colleagues

* Subhan (subhan.ichwan@sinarmas-distribusi.com)
* Mita (mita.ariani@sinarmas-distribusi.com)
* Theo (theo.damenta@sinarmas-distribusi.com)
* Ricardo (ricardo.silalahi@sinarmas-distribusi.com)
* Wisnu (wisnu.prayogo@bizzy.co.id)

## Project Managers & Owner

* Felix (felix.lu@bizzy.co.id, Group Product Manager)
* Gloria (glorianna.chrysilla@bizzy.co.id)
* Wiltonson (wiltonson.a.gunawan@sinarmas-distribusi.com)
* Theodora Emily (theodora.wattimena@bizzy.co.id)
* Nadia (nadia.nugroho@bizzy.co.id)
* Batara (batara.k.irmawan@sinarmas-distribusi.com)
* Ivan (ivan.teofilus@sinarmas-distribusi.com)
* Adi (kamryadi.arifin@bizzy.co.id)

## Scrum Master
* Andreas (andreas.caesareza@bizzy.co.id)
* Calina (calina.calina@bizzy.co.id)

## QA
* Wira (wirapa.pillay@bizzy.co.id, Head QA)
