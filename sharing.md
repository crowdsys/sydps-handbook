# Schedule & Topic

##### 28 Agustus 2019

- Generate APK menggunakan Bitbucket Pipelines
 - Presenter: Andreas

##### 11 September 2019

- Implementasi AWX dan Ansible
 - Presenter: Hardy/Ayu
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/451510888/[Ansible]+Install+AWX+Ansible+in+Container
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/587202632/[Ansible]+Configure+AWX
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/587137224/[Ansible]+Add+Playbook+in+AWX

##### 18 September 2019

- Telegram Alert (Bot)
 - Presenter: Dado/Ayu
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/701956346/Bitbucket+Alert+to+Telegram
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/702021913/Grafana+Alert+to+Telegram+Bot
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/715194686/RDS+Event+Notifier+to+Telegram+Bot
  - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/715293012/Deployment+Notification+to+Telegram

##### 25 September 2019

- Implementasi Flyway 
 - Presenter: Dado
 - https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/702054628/Flyway+Implement

##### 2 October 2019

- Build Dockerfile menggunakan Bitbucket Pipelines
 - Presenter: Ayu/Mirasz
 - https://bitbucket.org/crowdsys/dockerfile/src/master/README.md
