# Infrastructure as Code

## Solution

- [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)
- [Ansible AWX](https://github.com/ansible/awx)

## Provision EC2 Instances & RDS Cluster

- `git clone https://username@bitbucket.org/crowdsys/sysdevops-iac.git`
- `git checkout master`

## State File

- cat backend.tf | grep key
 - `key		= "terraform/sysdevops-iac/terraform.tfstate"` 

## Installing Necessary & By Request Services

- [AWX](http://ansible.tokosmart.id:8181/)

Contact:

- Hardy (hardy.agustian@bizzy.co.id)
- Ade (ade.hendrata@bizzy.co.id)

## Runbook

- `git clone https://username@bitbucket.org/crowdsys/sysdevops-iac.git`
- `git checkout -b TICKET-ID`
- `vim terraform.tfvars` (modify **AWS Access key ID** & **AWS Secret access**, set region to **ap-southeast-1**)
- `cp ec2_dev-cms-util.tf ec2_dev-new-instance.tf` (new instance for **Development Environment**)
- `cp ec2_prod-cms-util.tf ec2_prod-new-instance.tf` (new instance for **Production Environment**)
- `cp rds_dev-distribution-crows0.tf rds_dev-new-clustername.tf` (new instance for **Development Environment**)
- `cp rds_prod-distribution-crows0.tf rds_prod-new-clustername.tf` (new instance for **Production Environment**)
- `vim newfiles.tf` (modify all necessary resources name)
- `vim newfiles.tf` (modify `tags` key called `Ticket` set its value based on TICKET-ID)
- `terraform plan`
- `terraform apply`
- `git add newfiles.tf`
- `git commit -m "your comment here"`
- `git push origin TICKET-ID` (followed by create pull request to branch `master` and add **mirasbz** as reviewer)

### Important Notes

- always read `README.md` file from the repository
- `terraform show` to check latest provision information
- terraform state file (`tfstate`) are stored in S3
- delete `backend.tf` file if you want to test script in another region
- create new `.tf` file for newly provisioned EC2 instance or RDS cluster
- always ask for ticket reference to fill the instance's key/tag value
- always create a playbook or new requirements of services changes or similar
- please move code of terminated instance to `_terminated/` folder

## Codes from Pyco for Provision SSA Infrastructure

- `git clone https://username@bitbucket.org/crowdsys/prod-infrastructure.git`
- `git clone https://username@bitbucket.org/crowdsys/prod-ecs-alb-asg.git`

### Important Variables

* `terraform.tfvars`
  * AWS Access key ID
  * AWS Secret access

## Important Command

- `terraform init`
- `terraform plan`
- `terraform apply`
- `terraform destroy`
- `terraform show`

## Reading Materials

- [Install](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/442007609/How+to+install+terraform)
- [Provision Infrastructure](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/442040423/How+to+build+infrastructure)
- [Provision Application/Crowdsys Stack](https://sinarmas-distribusi.atlassian.net/wiki/spaces/CROW/pages/442171499/How+to+launch+application+in+ECS)
- [TFENV](https://sinarmas-distribusi.atlassian.net/wiki/spaces/SI/pages/722436131/Using+TFENV+to+Choose+Multi+Version+Terraform)
